import json
import os
import sys
import time
from enum import Enum
import networkx

import problem
from example import plan_breadth_first_search
from problem import Problem, Plan, load_problem, save_plan, Solution, TaskType, Task
from problem import plan_conflicting


class SimpleTask:
    def __init__(self, agent, box, node_start, node_end):
        self.agent = agent
        self.box = box
        self.node_start = node_start
        self.node_end = node_end

    def __str__(self):
        return f"Simple Task of agent {self.agent} to move box {self.box} from {self.node_start} to {self.node_end}"

    def __repr__(self):
        return self.__str__()


def plan_problem(problem: Problem) -> Plan:
    """
    Calculates a plan for the given problem.
    """
    # return plan_breadth_first_search(problem)

    cycles = networkx.simple_cycles(problem.graph)
    longest_length = 0
    longest_cycle = None

    for cycle in cycles:
        if len(cycle) > longest_length:
            longest_length = len(cycle)
            longest_cycle = cycle
    # longest_cycle.reverse()

    current_state = problem.state.clone()
    #
    split_graph = problem.graph.copy()
    for node in longest_cycle:
        for node2 in longest_cycle:
            try:
                split_graph.remove_edge(node, node2)
            except:
                pass


    for key, node in enumerate(longest_cycle):
        split_graph.add_edge(node, longest_cycle[(key + 1) % longest_length])
    networkx.write_graphml(split_graph, "problems/logistic_removed/graph.xml")



    new_state = current_state.clone()
    tasks_loop = []
    for t in range(1000):
        print('-' * 20)
        for key, node in enumerate(longest_cycle):
            print(f"current state: {current_state['a4']}")
            agent = current_state.agents_on_node(node)
            if agent:
                print(agent)
                agent = agent.pop()
                print(f"agent {agent} on node {node}")
                next_node = longest_cycle[(key + 1) % longest_length]
                if not current_state.agents_on_node(next_node):
                    tasks_loop.append(Task.move(agent, next_node))
                    # new_state[agent] = next_node
                else:
                    tasks_loop.append(Task.wait(agent, node))
            else:
                print(f"no agent on node {node}")

        for task in tasks_loop:
            current_state[task.agent] = task.node
        # current_state = new_state

    end_state = problem.state.clone()

    return task_list_to_plan(tasks_loop, problem.agents)


    bfs_n = 4
    intermediate_states = []
    for key, task in enumerate(problem.tasks):
        print(task)
        if task.type == TaskType.DROP:
            end_state[task.box] = task.node
            end_state[task.agent] = task.node
        if task.type == TaskType.PICK:
            end_state[task.box] = task.agent
            end_state[task.agent] = task.node
        if (key + 1) % bfs_n == 0:
            intermediate_states.append(end_state.clone())
            print(key)
    intermediate_states.append(end_state)

    sol = []
    previous_state = problem.state
    for state in intermediate_states:
        print(f"reaching state {state}")
        sol.extend(bfs(previous_state, state)[1])
        plan = task_list_to_plan(sol, problem.agents)
        save_plan("problems/deadlocks", plan)
        previous_state = state

    plan = task_list_to_plan(sol, problem.agents)
    return plan


def task_list_to_plan(task_list, agents):
    plan = Plan()
    for agent in agents:
        agent_tasks = plan.tasks_of(agent)
        for task in task_list:
            if task.agent == agent:
                agent_tasks.append(task)
    return plan


def bfs(start_state, end_state):
    state_list = [([start_state], [])]
    n_iter = 0
    traversed_states = set()
    traversed_states.add(hash_loc(start_state))
    while n_iter < 1000000:
        if n_iter % 10000 == 0:
            print(f"bfs iteration {n_iter}")
        # print('-' * 20)
        n_iter += 1
        current_tree = state_list.pop(0)
        next_states = get_next_states(current_tree[0][-1], list(start_state.agents))
        next_states.pop(0)
        # next_states.reverse()
        for next_state in next_states:
            state, tasks = next_state
            new_tasks = current_tree[1] + tasks
            new_statetree = current_tree[0].copy()
            new_statetree.append(state)
            if hash_loc(new_statetree[-1]) not in traversed_states:
                if new_statetree[-1] == end_state:
                    print('!' * 20)
                    print(new_statetree)
                    print(new_tasks)
                    return new_statetree, new_tasks
                state_list.append((new_statetree, new_tasks))
            traversed_states.add(hash_loc(state))
        # [print(tr) for tr in state_list]
    raise NotImplementedError()

def hash_loc(dict):
    return dict.__hash__()


def get_next_states(state_tasklist, agents_to_evaluate=None, timestep_origin_state=None):
    if type(state_tasklist) is not tuple:
        state_tasklist = (state_tasklist, [])
    if not timestep_origin_state:
        timestep_origin_state = state_tasklist[0]
    if agents_to_evaluate == []:
        return state_tasklist
    if not agents_to_evaluate:
        agents_to_evaluate = list(state_tasklist[0].agents)
    agent = agents_to_evaluate.pop(0)
    next_states = []
    new_tasks = []

    new_state = state_tasklist[0].clone()
    next_states.append(new_state)
    new_tasks.append(Task.wait(agent, state_tasklist[0][agent]))


    for step in state_tasklist[0].graph.neighbors(state_tasklist[0][agent]):
        if state_tasklist[0].agents_on_node(step) or timestep_origin_state.agents_on_node(step):
            continue
        if state_tasklist[0].boxes_on_agent(agent) and state_tasklist[0].boxes_on_node(state_tasklist[0][agent]):
            continue
        new_state = state_tasklist[0].clone()
        new_state[agent] = step
        next_states.append(new_state)
        new_tasks.append(Task.move(agent, step))

    if state_tasklist[0].boxes_on_agent(agent):
        new_state = state_tasklist[0].clone()
        box = list(state_tasklist[0].boxes_on_agent(agent))[0]
        new_state[box] = state_tasklist[0][agent]
        next_states.append(new_state)
        new_tasks.append(Task(TaskType.DROP, (agent, state_tasklist[0][agent], box)))
    if state_tasklist[0].boxes_on_node(state_tasklist[0][agent]) and (not state_tasklist[0].boxes_on_agent(agent)):
        new_state = state_tasklist[0].clone()
        box = list(state_tasklist[0].boxes_on_node(state_tasklist[0][agent]))[0]
        new_state[box] = agent
        next_states.append(new_state)
        new_tasks.append(Task(TaskType.PICK, (agent, state_tasklist[0][agent], box)))
    # print(agent)
    # print(next_states)
    large_list = [get_next_states((next_state, [next_task] + state_tasklist[1]),
                                  agents_to_evaluate=agents_to_evaluate.copy(),
                                  timestep_origin_state=timestep_origin_state) for next_state, next_task in
                  zip(next_states, new_tasks)]
    # print(large_list)
    combined_list = []
    if type(large_list[0]) == list:
        for l in large_list:
            combined_list.extend(l)
    else:
        combined_list = large_list
    # val = [item for sublist in large_list for item in sublist]
    # print(agent)
    # print(val)
    return combined_list


if __name__ == '__main__':
    def load_plan_save(problem_dir: str) -> float:
        problem = load_problem(problem_dir)
        if problem:
            print(f"-------- {os.path.basename(problem_dir)} --------")
            start = time.process_time()
            plan = plan_problem(problem) or Plan()
            duration = time.process_time() - start
            save_plan(problem_dir, plan)
            solution = Solution(problem, plan)
            print(solution.generateReport())
            print(f"Duration: {duration:.2f} s")
            print(f"Score: {solution.score()}")
            return solution.score()
        return 0


    if len(sys.argv) == 2:
        problem_dir = sys.argv[1]
        if not os.path.isdir(problem_dir):
            raise ValueError(f'Argument "{problem_dir}" is not a directory')
        load_plan_save(problem_dir)
    else:
        total = 0
        for problem_dir in [f.path for f in os.scandir('problems') if f.is_dir()]:
            total += load_plan_save(problem_dir)
        print(f"Total score: {total}")
